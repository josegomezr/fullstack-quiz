import React from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import { Container, Row, Col, ListGroup, ListGroupItem } from 'reactstrap';

// gathering views
import Greetings from './views/Greetings.jsx';
import QuestionOne from './views/QuestionOne.jsx';
import QuestionTwo from './views/QuestionTwo.jsx';
import QuestionThree from './views/QuestionThree.jsx';
import QuestionFour from './views/QuestionFour.jsx';
import QuestionFourCS from './views/QuestionFourCS.jsx';

// getting the store
import store from './store';
import { Provider } from 'react-redux';

// hooking up the routes
const routes = [
  {
    path: '/',
    exact: true,
    main: Greetings,
  },
  {
    path: '/question-1',
    main: QuestionOne,
  },
  {
    path: '/question-2',
    main: QuestionTwo,
  },
  {
    path: '/question-3',
    main: QuestionThree,
  },
  {
    path: '/question-4',
    main: QuestionFour,
  },
  {
    path: '/question-4-alt',
    main: QuestionFourCS,
  },
];

export default () => (
  <Provider store={store}>
    <Router>
      <Container className="pt-4">
        <Row>
          <Col xs="12" sm="3">
            <ListGroup>
              <ListGroupItem to="/" exact tag={NavLink}>
                Home
              </ListGroupItem>
              <ListGroupItem tag={NavLink} to="/question-1">
                Question One
              </ListGroupItem>
              <ListGroupItem tag={NavLink} to="/question-2">
                Question Two
              </ListGroupItem>
              <ListGroupItem tag={NavLink} to="/question-3">
                Question Three
              </ListGroupItem>
              <ListGroupItem tag={NavLink} to="/question-4">
                Question Four
              </ListGroupItem>
              <ListGroupItem tag={NavLink} to="/question-4-alt">
                Question Four (with-redux)
              </ListGroupItem>
            </ListGroup>
          </Col>
          <Col xs="12" sm="9">
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </Col>
        </Row>
      </Container>
    </Router>
  </Provider>
);


