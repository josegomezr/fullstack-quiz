process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');

const should = chai.should();

const server = require('../');
chai.use(chaiHttp);

describe('UC1: First Question', () => {
  describe('One Unique Country', () => {
    it('UC1-1: Should return a country object.', done => {
      chai
        .request(server)
        .get('/api/question-1/country-fullname')
        .query({
          name: 'Venezuela (Bolivarian Republic of)',
        })
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body).to.include({
            alpha2Code: 'VE',
            region: 'Americas',
            name: 'Venezuela (Bolivarian Republic of)',
          });
          done();
        });
    });
    it('UC1-2: Should return 404 if a country name is not found.', done => {
      chai
        .request(server)
        .get('/api/question-1/country-fullname')
        .query({
          name: 'Death Star',
        })
        .end((err, res) => {
          expect(res.status).to.be.equal(404);
          done();
        });
    });
  });
});
