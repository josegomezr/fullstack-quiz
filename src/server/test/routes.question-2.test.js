process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;

const should = chai.should();

const server = require('../');

chai.use(require('chai-http'));
chai.use(require('chai-subset'));

describe('UC2 Second Question', () => {
  describe('Countries by an array of names', () => {
    it('UC2-1 should return an empty array if no names.', done => {
      chai
        .request(server)
        .get('/api/question-2/country-partials')
        .query({
          names: '',
        })
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body).to.be.a('array');
          expect(res.body.length).to.be.equal(0);
          done();
        });
    });

    it('UC2-2 should return an array of countries matching partial name.', done => {
      chai
        .request(server)
        .get('/api/question-2/country-partials')
        .query({
          names: 'ven',
        })
        .end((err, res) => {
          expect(res.status).to.be.equal(200);

          expect(res.body).to.be.a('array');
          expect(res.body).to.containSubset([
            {
              alpha2Code: 'SI',
              region: 'Europe',
              name: 'Slovenia',
            },
            {
              alpha2Code: 'VE',
              name: 'Venezuela (Bolivarian Republic of)',
              region: 'Americas',
            },
            {
              alpha2Code: 'AX',
              name: 'Åland Islands',
              region: 'Europe',
            },
            {
              alpha2Code: 'SK',
              name: 'Slovakia',
              region: 'Europe',
            },
          ]);

          expect(res.body.length).to.be.equal(4);
          done();
        });
    });

    it('UC2-3 should merge results from more than one name.', done => {
      chai
        .request(server)
        .get('/api/question-2/country-partials')
        .query({
          names: 'venez,peru',
        })
        .end((err, res) => {
          expect(res.status).to.be.equal(200);

          expect(res.body).to.be.a('array');
          expect(res.body).to.containSubset([
            {
              alpha2Code: 'PE',
              region: 'Americas',
              name: 'Peru',
            },
            {
              alpha2Code: 'VE',
              name: 'Venezuela (Bolivarian Republic of)',
              region: 'Americas',
            },
          ]);

          expect(res.body.length).to.be.equal(2);
          done();
        });
    });
  });
});
