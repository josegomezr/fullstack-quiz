process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;

const should = chai.should();

const api = require('../lib/slot-machine');

chai.use(require('chai-subset'));

describe('UC3 Slot Machine', () => {
  describe('Rewards', () => {
    it('UC3-1 should return 0 if no winning match is found.', done => {
      var reward = api.getRewards(['cherry', 'lemon', 'apple']);
      expect(reward)
        .to.be.a('number')
        .equal(0);

      done();
    });

    it('UC3-2 should return 50 coins if 3 cherries appear in a row', done => {
      var reward = api.getRewards(['cherry', 'cherry', 'cherry']);
      expect(reward)
        .to.be.a('number')
        .equal(50);
      done();
    });
    it('UC3-3 should return 40 coins if 2 cherries appear in a row', done => {
      var reward = api.getRewards(['cherry', 'cherry', 'banana']);
      expect(reward)
        .to.be.a('number')
        .equal(40);
      done();
    });
    it('UC3-4 should return 20 coins if 3 apples appear in a row', done => {
      var reward = api.getRewards(['apple', 'apple', 'apple']);
      expect(reward)
        .to.be.a('number')
        .equal(20);
      done();
    });
    it('UC3-5 should return 10 coins if 2 apples appear in a row', done => {
      var reward = api.getRewards(['apple', 'apple', 'cherry']);
      expect(reward)
        .to.be.a('number')
        .equal(10);
      done();
    });
    it('UC3-6 should return 15 coins if 3 bananas appear in a row', done => {
      var reward = api.getRewards(['banana', 'banana', 'banana']);
      expect(reward)
        .to.be.a('number')
        .equal(15);
      done();
    });
    it('UC3-7 should return 5 coins if 2 bananas appear in a row', done => {
      var reward = api.getRewards(['cherry', 'banana', 'banana']);
      expect(reward)
        .to.be.a('number')
        .equal(5);
      done();
    });
    it('UC3-8 should return 3 coins if 3 lemons appear in a row', done => {
      var reward = api.getRewards(['lemon', 'lemon', 'lemon']);
      expect(reward)
        .to.be.a('number')
        .equal(3);
      done();
    });
  });

  describe('Rolling Mechanism', () => {
    it('UC3-9 should discount coins', done => {
      var randomFn = () => 1000;
      var machine = api.createSlotMachine(randomFn);

      expect(machine.coins).to.be.equal(20);
      machine.roll();
      expect(machine.coins).to.be.equal(19);
      machine.roll();
      machine.roll();
      machine.roll();
      expect(machine.coins).to.be.equal(16);
      done();
    });

    it('UC3-10 should increment coins on reward', done => {
      var randomFn = () => 1;
      var machine = api.createSlotMachine(randomFn);

      expect(machine.coins).to.be.equal(20);
      machine.roll();

      expect(machine.lastReward).to.be.equal(3);
      expect(machine.lastRoll).to.be.ordered.members([
        'lemon',
        'lemon',
        'lemon',
      ]);
      expect(machine.coins).to.be.equal(22);

      randomFn = () => 440;
      machine.random = randomFn; // HACK

      machine.roll();

      expect(machine.lastReward).to.be.equal(50);
      expect(machine.lastRoll).to.be.ordered.members([
        'cherry',
        'cherry',
        'cherry',
      ]);
      expect(machine.coins).to.be.equal(71);

      done();
    });
  });
});
