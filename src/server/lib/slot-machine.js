const get = require('lodash.get');

const FIRST_REEL = [
  'cherry',
  'lemon',
  'apple',
  'lemon',
  'banana',
  'banana',
  'lemon',
  'lemon',
];
const SECOND_REEL = [
  'lemon',
  'apple',
  'lemon',
  'lemon',
  'cherry',
  'apple',
  'banana',
  'lemon',
];
const THIRD_REEL = [
  'lemon',
  'apple',
  'lemon',
  'apple',
  'cherry',
  'lemon',
  'banana',
  'lemon',
];

function getRewards(reelRow) {
  var counts = reelRow.reduce(function(curr, fruit) {
    curr[fruit] = get(curr, fruit, 0) + 1;
    return curr;
  }, {});

  var reward = 0;

  if (get(counts, 'cherry', 0) == 3) {
    reward = 50;
  }

  if (get(counts, 'cherry', 0) == 2) {
    reward = 40;
  }

  if (get(counts, 'apple', 0) == 3) {
    reward = 20;
  }
  if (get(counts, 'apple', 0) == 2) {
    reward = 10;
  }

  if (get(counts, 'banana', 0) == 3) {
    reward = 15;
  }

  if (get(counts, 'banana', 0) == 2) {
    reward = 5;
  }

  if (get(counts, 'lemon', 0) == 3) {
    reward = 3;
  }

  return reward;
}

function getReelRow(first, second, third) {
  if (first > FIRST_REEL.length || first < 0) {
    throw Error('Incorrect value for reel:', first);
  }

  if (second > SECOND_REEL.length || second < 0) {
    throw Error('Incorrect value for reel:', second);
  }

  if (third > THIRD_REEL.length || third < 0) {
    throw Error('Incorrect value for reel:', third);
  }

  return [
    get(FIRST_REEL, first),
    get(SECOND_REEL, second),
    get(THIRD_REEL, third),
  ];
}

function SlotMachine(randomFn) {
  this.random = randomFn;
  this.coins = 20;
  this.reels = [FIRST_REEL, SECOND_REEL, THIRD_REEL];
  this.lastRoll = null;
  this.lastReward = 0;
}

SlotMachine.prototype.reset = function() {
  this.coins = 20;
  return this;
};

SlotMachine.prototype.roll = function() {
  if (this.coins == 0) {
    return false;
  }

  this.discountRoll();

  var numbers = this.random()
    .toString()
    .split('')
    .reverse();

  while (numbers.length < 3) {
    numbers.push('0');
  }

  var row = numbers
    .slice(0, 3)
    .map(parseFloat)
    .map((n, i) => {
      return get(this.reels[i], n % 8);
    });

  this.updateBalance(row);
  this.lastRoll = row;

  return {
    roll: this.lastRoll,
    reward: this.lastReward,
  };
};

SlotMachine.prototype.discountRoll = function() {
  this.coins -= 1;
  return this;
};

SlotMachine.prototype.updateBalance = function(row) {
  var reward = getRewards(row);
  this.coins += reward;
  this.lastReward = reward;
  return this;
};

module.exports = {
  getRewards,
  getReelRow,
  createSlotMachine: function(randomFn) {
    return new SlotMachine(randomFn);
  },
};
