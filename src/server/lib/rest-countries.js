const axios = require('axios');
const get = require('lodash.get');

const BASE_URL = `https://restcountries.eu/rest/v2`;

var log = function(...args) {
  if (process.env.NODE_ENV == 'production') {
    return
  }
  console.log.apply(console, args);
};

/*
 * Get a unique country
 *
 * Asumptions: 
 * - Only the first country returned from the backend will be used.
 * - Input is the country's full name, as in "Venezuela (Bolivarian Republic of)"
 */

async function countryByExactName(exactName) {
  exactName = encodeURIComponent(exactName);
  let url = `${BASE_URL}/name/${exactName}`;
  try {
    let response = await axios.get(url, {
      params: {
        fulltext: true,
      },
    });
    return response.data.shift();
  } catch (e) {
    if (e.response) {
      log('Request Error', e.response.data);
      return null;
    }
    log('Runtime Error', e);
    return null;
  }
}

/*
 * Get a list of countries by a partial name
 *
 * Asumptions: 
 * - Input is the country's partial or native, as explained in https://restcountries.eu/
 */
async function countriesByPartialName(partial) {
  let url = `${BASE_URL}/name/${partial}`;
  try {
    let response = await axios.get(url);
    return response.data;
  } catch (e) {
    if (e.response) {
      log('Request Error', e.response.data);
      return null;
    }
    log('Runtime Error', e);
    return null;
  }
}

module.exports = {
  countryByExactName,
  countriesByPartialName,
};
