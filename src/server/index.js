var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var Q1Router = require('./routes/question-1');
var Q2Router = require('./routes/question-2');
var Q4Router = require('./routes/question-4');

var app = express();

app.use(
  logger('dev', {
    skip: function(req, res) {
      return process.env.NODE_ENV == 'production';
    },
  })
);

app.set('trust proxy');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../../build')));

app.use('/api/question-1', Q1Router);
app.use('/api/question-2', Q2Router);
app.use('/api/question-4', Q4Router);

app.use('/', indexRouter);

module.exports = app;
