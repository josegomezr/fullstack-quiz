const express = require('express');
const router = express.Router();
const api = require('../lib/slot-machine');
const get = require('lodash.get');

var GameState = {}; // this should be on a service or something like that.

router.post('/roll', async function(req, res, next) {
  var ip = req.ip;
  var randomFn = () => (Math.random() * Date.now()) % 100 | 0;

  GameState[ip] = get(GameState, ip, api.createSlotMachine(randomFn));
  var currentMachine = GameState[ip];

  var result = currentMachine.roll();

  return res
    .json({
      roll: result,
      stats: {
        coins: currentMachine.coins,
      },
    })
    .end();
});

router.get('/stats', async function(req, res, next) {
  var ip = req.ip;

  var randomFn = () => (Math.random() * Date.now()) % 100 | 0;

  GameState[ip] = get(GameState, ip, api.createSlotMachine(randomFn));
  var currentMachine = GameState[ip];

  return res
    .json({
      stats: {
        coins: currentMachine.coins,
      },
    })
    .end();
});

router.post('/restart', async function(req, res, next) {
  var ip = req.ip;

  var randomFn = () => (Math.random() * Date.now()) % 100 | 0;

  GameState[ip] = get(GameState, ip, api.createSlotMachine(randomFn));
  var currentMachine = GameState[ip];
  currentMachine.reset();

  return res
    .json({
      stats: {
        coins: currentMachine.coins,
      },
    })
    .end();
});

module.exports = router;
