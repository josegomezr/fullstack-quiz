var express = require('express');
var router = express.Router();
var api = require('../lib/rest-countries');
/*
 * Get a unique country
 *
 * Asumptions: Only the first country returned from the backend will
 * be used.
 */
router.get('/country-fullname', async function(req, res, next) {
  var lookup = req.query.name;
  var country = await api.countryByExactName(lookup);

  var statusCode = country === null ? 404 : 200;
  return res
    .status(statusCode)
    .json(country)
    .end();
});

module.exports = router;
