var express = require('express');
var router = express.Router();
var api = require('../lib/rest-countries');

router.get('/country-partials', async function(req, res, next) {
  var { names = '' } = req.query;

  names = names.split(',').filter(function(name) {
    return !!name.trim();
  });

  var promises = names.map(async function(name) {
    var matches = api.countriesByPartialName(name);
    return matches;
  });

  promises = await Promise.all(promises);

  var searchResult = promises.reduce(function(current, next) {
    current = current.concat(next);
    return current;
  }, []);

  return res.json(searchResult).end();
});

module.exports = router;
