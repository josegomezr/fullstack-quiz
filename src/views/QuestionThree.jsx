import React from 'react';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import {
  Button,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  Row,
  Col,
  Table,
} from 'reactstrap';
import get from 'lodash.get';
import axios from 'axios';
import styles from './QuestionThree.module.css';

export default class QuestionThree extends React.Component {
  setFormRef = ref => {
    this.form = ref;
  };

  render() {
    const { filteredCountries, filter } = this.state;
    return (
      <div>
        <h2>Client-side search</h2>

        <AvForm ref={this.setFormRef} onValidSubmit={this.handleValidSubmit}>
          <AvGroup>
            <Label for="filter">Filter</Label>
            <Row form>
              <Col>
                <AvInput
                  name="filter"
                  id="filter"
                  type="text"
                  placeholder={'Fuzzy search, look for "co"...'}
                />
              </Col>
              <Col xs="auto">
                <Button color="primary">Search</Button>
              </Col>
            </Row>
          </AvGroup>
        </AvForm>
        {/* I'm sure there are more delicate ways to do this... */}
        {filter &&
          filteredCountries &&
          filteredCountries.length > 0 && (
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Capital</th>
                  <th>Flag</th>
                </tr>
              </thead>
              <tbody>
                {filteredCountries.map((country, index) => (
                  <tr key={index}>
                    <th scope="row">{country.name}</th>
                    <td>{get(country, 'capital') || 'N/A'}</td>
                    <td className="text-center">
                      <img
                        alt="flag"
                        className={styles.tableFlag}
                        src={country.flag}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          )}

        <Modal isOpen={this.state.loading}>
          <ModalHeader toggle={this.closeModal}>Loading...</ModalHeader>
          <ModalBody>
            <p>Fetching data...</p>
          </ModalBody>
        </Modal>
      </div>
    );
  }

  state = {
    filter: '',
    allCountries: null,
    filteredCountries: [],
    loading: true,
  };

  async componentDidMount() {
    try {
      var { data } = await axios.get('https://restcountries.eu/rest/v2/all');
    } catch (e) {
      this.form.setError('filter');
    } finally {
      this.setState({
        ...this.state,
        allCountries: data,
        loading: false,
      });
    }
  }

  filterCountries = async () => {
    if (!this.state.allCountries) {
      this.setState({
        ...this.state,
        filteredCountries: [],
      });
      return;
    }

    var regex = new RegExp(this.state.filter.toLowerCase(), 'ig');

    var filtered = this.state.allCountries.filter(country => {
      var fields = ['name', 'alpha2Code', 'alpha3Code', 'capital'];

      return fields
        .map(field => (get(country, field) + '').toLowerCase())
        .reduce((valid, next) => valid || regex.test(next), false);
    });

    this.setState({
      ...this.state,
      loading: false,
      filteredCountries: filtered,
    });
  };
  handleValidSubmit = async (event, values) => {
    this.setState({
      filter: values.filter,
      loading: true,
    });
    this.filterCountries();
  };
}
