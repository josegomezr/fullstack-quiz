import React from 'react';

import {} from 'reactstrap';

export default () => (
  <div>
    <h2>Hi there!</h2>

    <p>
      This is my humble attempt to apply for{' '}
      <a href="https://gitlab.com/snippets/1762466">this</a> test.
    </p>

    <p>Feel free to navigate the questions in the sidebar.</p>

    <p>
      <strong>Contact Info</strong>
    </p>

    <div>
      <strong>Email: </strong>
      <a href="mailto:1josegomezr@gmail.com">1josegomezr@gmail.com</a>
    </div>
    <div>
      <strong>Telegram: </strong>
      <a href="https//t.me/josegomezr">@1josegomezr</a>
    </div>
  </div>
);
