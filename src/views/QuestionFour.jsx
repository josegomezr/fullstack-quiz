import React from 'react';

import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import get from 'lodash.get';

import axios from 'axios';

export default class QuestionFour extends React.Component {
  setFormRef = ref => {
    this.form = ref;
  };

  render() {
    const { slotStats, lastRoll } = this.state;
    return (
      <div>
        <h2>Slot Machine!</h2>

        <div>
          <strong>Available Coins: </strong>
          <span>{get(slotStats, 'coins')} Coins</span>
        </div>

        <div>
          <strong>Last Roll: </strong>
          {lastRoll && <span>{get(lastRoll, 'roll', []).join(', ')}</span>}
          {!lastRoll && <span>Haven't rolled yet!</span>}
        </div>

        <div>
          <strong>Last Reward: </strong>
          {lastRoll && <span>{get(lastRoll, 'reward', 0)} Coins</span>}
          {!lastRoll && <span>Haven't rolled yet!</span>}
        </div>

        <Button onClick={this.doRoll}>Roll!</Button>

        <Modal isOpen={this.state.loading}>
          <ModalHeader toggle={this.closeModal}>Loading...</ModalHeader>
          <ModalBody>
            <p>Rolling rolling rolling...</p>
          </ModalBody>
        </Modal>
      </div>
    );
  }

  state = {
    slotStats: null,
    lastRoll: null,
    loading: true,
  };

  doRoll = async () => {
    this.setState({
      ...this.state,
      loading: true,
    });
    var { data } = await axios.post('/api/question-4/roll');
    this.setState({
      ...this.state,
      slotStats: data.stats,
      loading: false,
      lastRoll: data.roll,
    });
  };

  async componentDidMount() {
    try {
      var { data } = await axios.get('/api/question-4/stats');
    } catch (e) {
      this.form.setError('filter');
    } finally {
      this.setState({
        ...this.state,
        slotStats: data.stats,
        loading: false,
      });
    }
  }
}
