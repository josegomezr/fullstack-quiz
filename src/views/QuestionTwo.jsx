import React from 'react';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import {
  Button,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  Row,
  Col,
  Table,
} from 'reactstrap';
import get from 'lodash.get';
import axios from 'axios';
import styles from './QuestionTwo.module.css';

export default class QuestionTwo extends React.Component {
  setFormRef = ref => {
    this.form = ref;
  };

  render() {
    const { countries } = this.state;
    return (
      <div>
        <h2>Partial name search</h2>

        <AvForm ref={this.setFormRef} onValidSubmit={this.handleValidSubmit}>
          <AvGroup>
            <Label for="countriesNames">Countries' names</Label>
            <Row form>
              <Col>
                <AvInput
                  name="countriesNames"
                  id="countriesNames"
                  type="text"
                  placeholder={
                    'List of partial names, something like "Peru, Colomb" or "spai,united"'
                  }
                  required
                />
              </Col>
              <Col xs="auto">
                <Button color="primary">Search</Button>
              </Col>
            </Row>
          </AvGroup>
        </AvForm>

        {countries && (
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>Name</th>
                <th>Capital</th>
                <th>Flag</th>
              </tr>
            </thead>
            <tbody>
              {countries.map((country, index) => (
                <tr key={index}>
                  <th scope="row">{country.name}</th>
                  <td>{get(country, 'capital') || 'N/A'}</td>
                  <td className="text-center">
                    <img
                      alt="flag"
                      className={styles.tableFlag}
                      src={country.flag}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}

        <Modal isOpen={this.state.loading}>
          <ModalHeader>Loading...</ModalHeader>
          <ModalBody>
            <p>Fetching data...</p>
          </ModalBody>
        </Modal>
      </div>
    );
  }

  state = {
    countriesNames: '',
    countries: null,
    loading: false,
  };

  handleValidSubmit = async (event, values) => {
    this.setState({
      ...this.state,
      loading: true,
    });
    var data = null;
    try {
      var response = await axios.get('/api/question-2/country-partials', {
        params: {
          names: values.countriesNames.trim(),
        },
      });
      data = response.data;
    } catch (e) {
      this.form.setError('countriesNames');
    } finally {
      this.setState({
        ...this.state,
        countries: data,
        loading: false,
      });
    }
  };
}
