import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchStats, roll } from '../store/actions';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import get from 'lodash.get';

export class QuestionFourCS extends React.Component {
  setFormRef = ref => {
    this.form = ref;
  };

  render() {
    const { slotStats, lastRoll, loading } = this.props;
    return (
      <div>
        <h2>Slot Machine!</h2>

        <div>
          <strong>Available Coins: </strong>
          <span>{get(slotStats, 'coins')} Coins</span>
        </div>

        <div>
          <strong>Last Roll: </strong>
          {lastRoll && <span>{get(lastRoll, 'roll', []).join(', ')}</span>}
          {!lastRoll && <span>Haven't rolled yet!</span>}
        </div>

        <div>
          <strong>Last Reward: </strong>
          {lastRoll && <span>{get(lastRoll, 'reward', 0)} Coins</span>}
          {!lastRoll && <span>Haven't rolled yet!</span>}
        </div>

        <Button onClick={this.doRoll}>Roll!</Button>

        <Modal isOpen={loading}>
          <ModalHeader>Loading...</ModalHeader>
          <ModalBody>
            <p>Rolling rolling rolling...</p>
          </ModalBody>
        </Modal>
      </div>
    );
  }

  doRoll = async () => {
    this.props.roll();
  };

  async componentDidMount() {
    this.props.fetchStats();
  }
}

const mapStateToProps = state => ({
  loading: state.loading,
  slotStats: state.stats,
  lastRoll: state.lastRol,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchStats,
      roll,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionFourCS);
