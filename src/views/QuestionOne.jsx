import React from 'react';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import {
  Button,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  Row,
  Col,
} from 'reactstrap';
import get from 'lodash.get';

import axios from 'axios';

import styles from './QuestionOne.module.css';

export default class QuestionOne extends React.Component {
  setFormRef = ref => {
    this.form = ref;
  };

  render() {
    const { countryInfo } = this.state;
    return (
      <div>
        <h2>Full name search</h2>

        <AvForm ref={this.setFormRef} onValidSubmit={this.handleValidSubmit}>
          <AvGroup>
            <Label for="countryName">Country's Full Name</Label>
            <Row form>
              <Col>
                <AvInput
                  name="countryName"
                  id="countryName"
                  type="text"
                  placeholder={'Something like "Perú" or "Slovenia"'}
                  required
                />
              </Col>
              <Col xs="auto">
                <Button color="primary">Search</Button>
              </Col>
            </Row>
          </AvGroup>
        </AvForm>
        <Modal isOpen={!!countryInfo} toggle={this.closeModal}>
          <ModalHeader toggle={this.closeModal}>
            {get(countryInfo, 'name')}
          </ModalHeader>
          <ModalBody>
            <Media>
              <Media left href="#">
                <Media
                  className={styles.flagIcon}
                  object
                  src={get(countryInfo, 'flag')}
                  alt="Generic placeholder image"
                />
              </Media>
              <Media body>
                <div>
                  <strong>Capital: </strong>
                  <span>{get(countryInfo, 'capital')}</span>
                </div>

                <div>
                  <strong>ISO Names:</strong>
                  <span>
                    {get(countryInfo, 'alpha2Code')},{' '}
                    {get(countryInfo, 'alpha3Code')}
                  </span>
                </div>

                <div>
                  <strong>Region:</strong>
                  <span>{get(countryInfo, 'region')}</span>
                </div>
              </Media>
            </Media>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.closeModal}>
              Close
            </Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.loading}>
          <ModalHeader toggle={this.closeModal}>Loading...</ModalHeader>
          <ModalBody>
            <p>Fetching data...</p>
          </ModalBody>
        </Modal>
      </div>
    );
  }

  state = {
    countryName: '',
    countryInfo: null,
    loading: false,
  };

  handleValidSubmit = async (event, values) => {
    this.setState({
      ...this.state,
      loading: true,
    });
    var data = null;
    try {
      var response = await axios.get('/api/question-1/country-fullname', {
        params: {
          name: values.countryName.trim(),
        },
      });
      data = response.data;
    } catch (e) {
      this.form.setError('countryName');
    } finally {
      this.setState({
        ...this.state,
        countryInfo: data,
        loading: false,
      });
    }
  };

  closeModal = () => {
    this.setState({ countryName: '', countryInfo: null });
  };
}
