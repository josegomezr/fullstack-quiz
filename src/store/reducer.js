const initialState = {
  stats: {
    coins: 0,
  },
  lastRoll: {
    roll: [],
    reward: 0,
  },
  loading: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case 'FETCHING_STATS':
    case 'ROLLING':
      return {
        ...state,
        loading: true,
      };
    case 'STORE_STATS':
      return {
        ...state,
        stats: action.payload.stats,
        loading: false,
      };
    case 'STORE_ROLL':
      return {
        ...state,
        stats: action.payload.stats,
        lastRoll: action.payload.roll,
        loading: false,
      };
    default:
      return state;
  }
}
