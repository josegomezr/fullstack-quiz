import axios from 'axios';

export const fetchStats = () => {
  return async dispatch => {
    dispatch({
      type: 'FETCHING_STATS',
    });

    var payload = {};
    try {
      var { data } = await axios.get('/api/question-4/stats');
      payload = data;
    } catch (e) {
      payload.error = e;
    } finally {
      dispatch({
        type: 'STORE_STATS',
        payload,
      });
    }
  };
};

export const roll = () => {
  return async dispatch => {
    dispatch({
      type: 'ROLLING',
    });

    var payload = {};
    try {
      var { data } = await axios.post('/api/question-4/roll');
      payload = data;
    } catch (e) {
      payload.error = e;
    } finally {
      dispatch({
        type: 'STORE_ROLL',
        payload,
      });
    }
  };
};
