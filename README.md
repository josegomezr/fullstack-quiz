# Install Instructions

Clone it, then:

```bash
npm i
npm run build
npm start # check http://localhost:3001
```

---

## SQL Section
### Question 1:

![DB Diagram](./db-diagram.png)

**Script:**
```sql
CREATE TABLE games(
  type VARCHAR(10) NOT NULL,
  name VARCHAR(10) NOT NULL,
  PRIMARY KEY(type)
);

CREATE TABLE players(
  -- id can be AUTO_INCREMENT in MySQL or SERIAL in PSQL
  -- I'm keeping it standard.
  id int NOT NULL,
  name VARCHAR(10) NOT NULL,
  favourite_game_type VARCHAR(10) NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (favourite_game_type) REFERENCES games(type)
);
```

### Question 2:

```sql
SELECT * FROM players
WHERE favourite_game_type = 'SLOT';
```

---

Assumptions:
- Question 1 search is performed by full country name as specified in [restcountries.eu](https://restcountries.eu/#api-endpoints-all)
- Question 2 search is performed by partial/native name provided by (again) [restcountries.eu](https://restcountries.eu/#api-endpoints-all)
- Question 3 search is done in client-side (in memory) after fetching all the countries first.
- Question 4 the slot machine login was implemented on the server and hooked up to the frontend.

Check it out live [here](https://rs3-fullstack-test.herokuapp.com/)

